import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Meta } from '@storybook/react/types-6-0';
import { SearchOutlined } from '@ant-design/icons';

import Button from '../components/button/button';

export default {
  title: 'Example/按钮',
  component: Button,
} as Meta;

const Template = (args: any) => {
  const { label, ...otherArgs } = args;

  return <Button {...otherArgs}>{label}</Button>;
};

export const Primary = Template.bind({});

Primary.args = {
  label: '按钮',
  ghost: false,
  block: true,
  loading: true,
  size: 'large',
};
